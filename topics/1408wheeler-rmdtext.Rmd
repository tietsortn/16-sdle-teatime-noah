---
title: "Nick Wheeler's Test R Markdown Document" 
output:
##  ioslides_presentation:
##    toc: yes
##    smaller: true
##    widescreen: true
##    fig_width: 6
##    fig_height: 4
  pdf_document:
    number_sections: yes
    toc: yes
##  beamer_presentation:
##    fig_height: 6
##    fig_width: 8
##    fontsize: 10pt
  html_document:
    toc: yes
  beamer_presentation:
    toc: yes

---
<!-- I guess this is how to put in a 'regular html comment'
which can take place on multiple lines? Yes it can!--> 

This is a test R Markdown file.

I will try out some of the new functionality here.

## Test 1 
Like outputing into `r 1 + 1` formats at once using the R command:
```
rmarkdown::render("1408wheeler-rmdtext.Rmd","all")
```
## Test 2 Font Formatting
And using formats like *italics* or **bold**.

What do \textbf{\LaTeX} environments look like, I wonder?

\begin{flushright}
here's some flushright text with a $\frac{1}{\infty}$ equation!
\end{flushright}


## Test 3 Tables
And here's a tabular environment!

\begin{tabular}{|l|l|}\hline
Age & Frequency \\ \hline
18--25  & 15 \\
26--35  & 33 \\
36--45  & 22 \\ \hline
\end{tabular}

## LaTeX conclusions, Eqn vs word environments
None of the Latex stuff seems to be compiling into the html file, but it goes into the pdf just fine.

So let's try some of the traditional R Markdown stuff like lists and squirelly formatting:

* Item 1 - Find more info about R Markdown through this [link](http://rmarkdown.rstudio.com/)
* Item 2~a,b~
    + Item 2a^21^ (btw some meta-info stuff about R Markdown v2 is [here](http://blog.rstudio.org/2014/06/18/r-markdown-v2/)...)
    + ~~Item 2b~~  

## Test 4 Evaluating R code chunks
And of course, evaluating R code chunks:

```{r fig.width=5, fig.height=3}
mydf <- data.frame("X" = seq(1,3,1),"Y" = seq(1,3,1))
plot(mydf,main = "Test Plot", col = seq(1,3,1),pch = 20,cex = 3,ylim = c(0,4),xlim = c(0,4))
```

For the pdf output, this text goes to the next page...

But for the html, it makes one long scrolling continuous page!

(Too bad the latex stuff doesn't show up in the html, and unfortunatley a quick search of the web isn't turning anything up... Something to look into later, as it would be very nice to use R Markdown as a way to include Latex formatting in a single scrolling html webpage)

## Test 5 More R code chunks
Oh well, let's try some more R code chunks!

```{r}
myfunc <- function(vec){
  retvec <- 3*vec
  return(retvec)
}
```
I can define the above function in it's own code chunk, but then later on down the line apply it to things defined in even earlier code chunks:
```{r fig.width=5, fig.height=3}
mydf <- cbind(mydf,myfunc(mydf[,2]))
colnames(mydf)[3] <- "Z"
plot(mydf[,1],mydf[,2],main = "Test Plot 2", col = seq(1,3,1),pch = 20,cex = 3,
  ylim = c(0,12),xlim = c(0,4),ylab = "Y",xlab = "X")
points(mydf[,1],mydf[,3],pch = 20,cex = 3,col = seq(4,6,1))
```

## Rmd Conclusions
R Markdown v2 is pretty cool!

![alt text](figs/thumbsup.png)


